import java.util.*;
public class Game {
    private Table table;
    private int row;
    private int col;
    private Player o;
    private Player x;
    Scanner scan = new Scanner(System.in);
    
    public Game(){
        o = new Player('O');
        x = new Player('X');
    }
    public void startGame(){
        table = new Table(o,x);
        
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game.");
    }
    public void showTable(){
        char data [][] = table.getData();
        for(int i = 0;i<3;i++){
            for(int j = 0; j<3; j++){
                System.out.print(" " +data[i][j]);
            }
            System.out.println();
        }
    }
    public void showTurn(){
        System.out.println("Turn of "+table.getCurrentPlayer().getName());
    }
    public boolean inputRowCol(){
        System.out.print("Please input Row and Column. : ");
        try{
            row = scan.nextInt();
            col = scan.nextInt();
            table.setRowCol(row, col);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public boolean inputContinue(){
        System.out.print("Continue? (y/n): ");
        String check = scan.next();
        if(check.equals("y")){
            return true;
        }else{
            return false;
        }   
    }
    public void showWin(int i){
        if(i>0){
            System.out.println("Draw.");
            System.out.println("O: Win: "+o.getWin()+" Lose: "+o.getLose()+" Draw: "+o.getDraw());
            System.out.println("X: Win: "+x.getWin()+" Lose: "+x.getLose()+" Draw: "+x.getDraw());
        }else{
            System.out.println("Player "+table.getWinner()+" Win");
            System.out.println("O: Win: "+o.getWin()+" Lose: "+o.getLose()+" Draw: "+o.getDraw());
            System.out.println("X: Win: "+x.getWin()+" Lose: "+x.getLose()+" Draw: "+x.getDraw());
        }
    }
    public void showBye(){
        System.out.println("Bye Bye, sir.");
    }
    public void play(){
        showWelcome();
        do{
            startGame();
            playMore();
        }while(inputContinue());
        showBye();
    }
    public void playMore(){
        int i = 0;
        while (true) {
            showTable();
            showTurn();
            if (this.inputRowCol()) {
                if (table.checkWin()) {
                    showTable();
                    showWin(i);
                    return;
                }else if (table.checkDraw()) {
                    i++;
                    showTable();
                    showWin(i);
                    return;
        }}}
    }
}